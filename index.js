fetch('http://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

//Getting all TO DO LIST ITEM
fetch('http://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);
})

// Getting a specific to do list item
fetch('http://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

//Create a to do list item using POST method
fetch('http://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//Update a to do list item using PUT method
fetch('http://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure.",
		status: "Pending",
		dateCompleted: 'Pending',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Updtaing a to do list item using PATCH method
fetch('http://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure.",
		status: 'Complete',
		dateCompleted: '07/09/21',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Delete a to do list item

fetch('http://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'